require 'rack'

server = Rack::Builder.new do
  run Rack::Directory.new(Dir.pwd)
end

Rack::Handler::WEBrick.run(server, :Port => 8000, :BindAddress => "0.0.0.0")
#Rack::Handler::WEBrick.run(server, :Port => 8000, :BindAddress => "0.0.0.0", :DoNotListen => true)

#trap 'INT' do server.shutdown end
#https://rubular.com/r/zJqTms5XTE
#https://www.sitepoint.com/automatically-reload-things-guard/
#https://apidock.com/ruby/WEBrick
#server.start
#sudo gem install bundler -v 1.15.0.pre.3
