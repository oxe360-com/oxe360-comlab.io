---
title: OSSE/ODOO Enterprise
toc: MiTiTleTOC
---

Este es el módulo básico de localización contable peruana, cedido a ODOO para que sea asimilado como parte estándar de la a partir de la versión 12 en adelante.
Este módulo será instalado como parte de la contabilidad, siempre y cuando se escoja Perú al momento de iniciar la BD de nuestra empresa, es importante ya que en caso no se haya establecido un código del país o no se haya encontrado un módulo de localización, el módulo de localización l10n_generic_coa (US) se instala de forma predeterminada.

                                                     FEATURES

*   [Carga del Plan de Cuentas](Carga-del-Plan-de-Cuentas)
*   [Creación de Properties Plan de Cuentas](Creacion-de-Properties-Plan-de-Cuentas)
*   [Carga del Impuestos](Carga-de-Impuestos)
*   [Seteo de Perú como País por defecto en Partners](Seteo-de-Perú-como-País-por-defecto-en-Partners)
*   [Validar que el doc_number por doc_type sea correcto](Valida-que-el-doc_number-por-doc_type-sea-correcto)
*   [Seteo de doc_number en Partners de prueba](Seteo-valores-en-doc_number)
*   [Estructura de Ubigeo](Estructura-de-ubigeo)
*   [Tipos de Documento y Datos de Contacto](Tipos-de-Documento-y-datos-de-contacto-SUNAT)
*   [Validando Dependecias entre Pais - Departamento - Provincia - Distrito](Validando Dependecias entre Pais-Departamento-Provincia-Distrito)
