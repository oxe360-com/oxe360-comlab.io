En la clase **res.partner**, el metodo ***_check_doc_number***, se activa mediante un contrain en doc_number, este a su vez llama a ***_check_doc_number_and_doc_type***, que realiza la validación del número de documento, tanto para DNI como para RUC.
> DNI : se valida que sea numérico y la longitud de 8 digitud.

> RUC :  para su vaidación utilizamos el factor 5432765432.

            for i in range(0, 10):
                lsum += int(factor[i]) * int(doc_number[i])
            subtraction = 11 - (lsum % 11)

            if subtraction == 10:
                dig_check = 0
            elif subtraction == 11:
                dig_check = 1
            else:
                dig_check = subtraction

  Este código evalúa los primero 10 dígitos del RUC , al final se obtiene un dig_check, el cual debe ser igual al 11 digito del ruc(numero de varificación), en caso ser iguales el ruc es correcto.

![screenshot-localhost_8120-2019-02-21-12-33-46_1_](uploads/de4c6a7ebfa00093ce6ff756b1eacc54/screenshot-localhost_8120-2019-02-21-12-33-46_1_.png)
