---
title: Estructura de Ubigeo
toc: Estructura de ubigeo
---
Por defecto odoo llega hasta estados, como es sabido nuestra republicas, básicamente por, Departamentos(state), Provincias y Distritos, cada uno de los cuales cuentan con un código concatenado al nivel superior del cual proviene, el nivel mas bajo, en este caso  Distrito, constituye el UBIGEO.
Tanto los códigos de UBIGEO como las lista oficial de distritos, son proporcionados por el INEI(Instituto Nacional de estadistica e Informática) y actualizados periódicamente.
>  * res.state.province  : Este modelo almacenara las Provincias y depende del Estado(Departamento)
>
>| modelo | propiedad |
>| ------ | ------ |
>|res.state.province|	name|
>|res.state.province|	code|
>|res.state.province|	state_id|
>|res.state.province|	district_ids|

>  * res.province.district  : Este modelo almacenara los distritos y depende de la Provincia
>
>| modelo | propiedad |
>| ------ | ------ |
>|res.province.district|	name|
>|res.province.district|	code|
>|res.province.district|	province_id|

*  Nuestros registro de contacto(company/partner) está localizado para Perú, incluye la ubicación de la dirección en  el formato de departamento propio del país, con un código de ubigeo obtenido del distrito.
se adicionan los siguientes atributos a las modelos res.company	y, res.partner.

| modelo | propiedad/metodo |
| ------ | ------ |
| res.company	 | ruc |
| res.company	 | province_id |
| res.company	 | district_id |
| res.company	 | ubigeo |
| res.partner | province_id |
| res.partner	 | district_id |
| res.partner	 | ubigeo |

*  Los campos son agregado en la vista res_partner_form_l10n_pe_view, heredando de base.view_partner_form.

![screenshot-localhost_8120-2019-02-20-17-31-42](uploads/2a6e1aa3e181b677af0340344a69175b/screenshot-localhost_8120-2019-02-20-17-31-42.png)

