---
title: Creacioon de Impuestos
toc: creacion-de-impuestos
---

Es importante conocer lo modelos que actúan en la carga de los impuestos:
1.  account_tax_template: Los impuesto mediante el XML son cargados directamente en este modelo, al igual que el modelo
2.  account_account_template, no es consumido en los procesos contables, su utilidad es como plantilla para otros modelos.
3.  account_tax_group: Modelo utilizado para clasificar los impuestos.
4.  account_tax: Es el modelo utilizado en los procesos de la empresa, al igual que el caso de el account_account, copia la data desde el template.

**account_tax_group :**

 Modelo utilizado para clasificar los impuestos.

![screenshot-localhost_8120-2019-02-21-10-53-57](uploads/e089d2cf1496e1ea8ca73aff766971d5/screenshot-localhost_8120-2019-02-21-10-53-57.png)

**account.tax :**

Es el modelo utilizado en los procesos de la empresa, al igual que el caso de el account_account, copia la data desde el template

![screenshot-localhost_8120-2019-02-21-10-58-38](uploads/49f3c4fc0f13db8b116c23d26756bd9d/screenshot-localhost_8120-2019-02-21-10-58-38.png)
